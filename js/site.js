//**************************************
//  3Wcreative Web Design
//  https://www.facebook.com/3wcreative
//  Updated: 2017/06/06
//**************************************
var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
var Pad = /iPad/i.test(navigator.userAgent);


$(document).ready(function(){

	setTimeout("startAnim()", 1000)
	
	// navigaiton
	var wrap = $('.siteWrap');
	$('.btnMenuBur').on('click', function() {
	    if ( wrap.hasClass('menu-on') ) {
	        wrap.removeClass('menu-on');
	    } else {
	        wrap.addClass('menu-on');
	    } 
	});
	$('.triClose').on('click', function() {
	    if ( wrap.hasClass('menu-on') ) {
	        wrap.removeClass('menu-on');
	    } else {
	        wrap.addClass('menu-on');
	    } 
	});

});

function startAnim(){
	$('.loading.startAnim').removeClass('startAnim');
}